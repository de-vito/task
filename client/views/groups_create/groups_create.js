Template.createGroup.events({
	'click #create-new-group' :function (e, tmpl) {
		e.preventDefault();
		var groupName = tmpl.find('#name-of-new-group').value;
		var adminId  = Meteor.userId();
		groups.insert({groupname : groupName, admin :adminId});
		Router.go('homepage');
	}
	
})