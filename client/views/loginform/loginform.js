if(Meteor.isClient){
	Template.registration.events({
		'click #submit-registration':function (e, tmpl) {
			e.preventDefault();
			var userVar=tmpl.find('#username').value;
			var emailVar=tmpl.find('#useremail').value;
			var passVar=tmpl.find('#userpassword').value;
			
			Accounts.createUser({
				username: userVar,
				email: emailVar,
				password: passVar
			});
			Router.go('homepage');
		}
	});
		Template.login.events({
		'submit form':function (e, tmpl) {
			e.preventDefault();
			var emailVar=tmpl.find('#useremail-login').value;
			var passVar=tmpl.find('#userpassword-login').value;
			Meteor.loginWithPassword(emailVar,passVar);
		}
	});
		Template.logout.events({
			'click .logout':function (e) {
				e.preventDefault();
				Meteor.logout();
				Router.go('homepage')
			}
		});
		Template.header.events({
			'click .gotoregistration': function (e, tmpl) {
				e.preventDefault();
				Router.go('registration');
			}
		})
}

